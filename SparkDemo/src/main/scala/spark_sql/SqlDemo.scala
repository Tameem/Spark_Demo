package spark_sql

import org.apache.spark.sql.SparkSession

/**
  * Created by tameemum on 3/27/17.
  */
object SqlDemo {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession
      .builder()
      .master("local")
      .appName("Spark SQL basic example")
      .config("spark.some.config.option", "some-value")
      .getOrCreate()

    // For implicit conversions like converting RDDs to DataFrames
    import spark.implicits._


    val df = spark.read.json("/Users/tameemum/Downloads/spark-2.1.0-bin-hadoop2.7/examples/src/main/resources/people.json")

    // Displays the content of the DataFrame to stdout
    df.show()

    df.printSchema()

    df.select("name").show()

    df.select($"name",$"age"+1).show()

    df.filter($"age" > 20).show()

    df.groupBy("age").count().show()
  }

}
